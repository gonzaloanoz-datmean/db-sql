DROP DATABASE IF EXISTS empresapruebadatmean;
CREATE DATABASE empresapruebadatmean;
USE empresapruebadatmean;
DROP TABLE IF EXISTS empleado;
DROP TABLE IF EXISTS departamento;
CREATE TABLE departamento(
	IDDepartamento int AUTO_INCREMENT,
	departamento NVARCHAR(100) NOT NULL,
	PRIMARY KEY (IDDepartamento)
);
CREATE TABLE empleado(
	IDEmpleado int AUTO_INCREMENT,
	nombre NVARCHAR(50) NOT NULL,
	apellidos NVARCHAR(50) NOT NULL,
	edad int NOT NULL,
	sexo enum('Mujer','Hombre','No contesta'),
	departamento int NOT NULL,
	fechaContratacion DATE NOT NULL,
	PRIMARY KEY (IDEmpleado),
	FOREIGN KEY (departamento) REFERENCES departamento(IDDepartamento)
);

INSERT INTO departamento(departamento) VALUES ("Administración");
INSERT INTO departamento(departamento) VALUES ("Ventas");
INSERT INTO departamento(departamento) VALUES ("Márketing");
INSERT INTO departamento(departamento) VALUES ("Tecnología");
