# Prueba Datmean 

Proyecto para prueba técnica de Datmean:  de creación de servicio de registro de usuarios

# Las ramas llevarán los siguientes nombre

```
master -> rama principal y producción
develop -> rama estable de desarrollo y previa a master
feature/frontend_* -> nueva funcionalidad del front, añadir al final una palabra clave de la rama
feature/backend_* -> nueva funcionalidad del back, añadir al final una palabra clave de la rama
hotfix -> cambio en master (solo si tenemos un fallo cerrando entrega)
refactor/frontend_* -> refactorizaciones sobre el código front que no aportan nueva funcionalidad
refactor/backend_* -> refactorizaciones sobre el código back que no aportan nueva funcionalidad
```

# Estado actual del proyecto

Se ha priorizado el backend con la intención de la extracción de datos desde la DB mysql. Esta extracción no ha sido viable y esta en proceso de corrección. Las primeras pruebas en el back se realizaron con .ejs pero se han eliminado al no ser objetivo de presentación de este proyecto.

El frontend, realizado en VUE está en un estado inicial (vacío/comentado).

backend     -> git clone git@gitlab.com:gonzaloanoz-datmean/backend.git
                -> rama más avanzada: feature/dbconnection
frontend    -> git clone git@gitlab.com:gonzaloanoz-datmean/frontend.git
                -> rama más avanzada: tanto master como develope estan en el mismo commit.
db-sql      -> git clone git@gitlab.com:gonzaloanoz-datmean/db-sql.git
                -> solo presenta rama master


# Orden de ejecución

Una vez clonados los 3 repositorios y ubicado en la rama más avanzada:
    1. Creación de db mediante cliente mysql con el documento .sql presente en "db-sql"
    2. Modificación de los permisos en el backend de acceso a la db. En la carpeta de configuración -> dbConnection.js -> introducir un usuario local autorizado. - Esto es necesario para las pruebas en localhost -
    3. Lanzar comandos npm install tanto en backend como en frontend.
    4. En backend lanzar el comando nodemon src/index.js
    5. En frontend lanzar el comando npm run dev - Actualmente no hay nada funcional en esta parte.

    Como he indicado previamente esto no funcionará al no estar finalizado el proceso.

#Siguientes iteraciones

    1. Arreglar la llamada a mysql desde backend
    2. Mostrar los datos retornados en frontend
    3. Comprobar comportamiento injesta de nuevos datos desde backend (posiblemente con mismo fallo en el post que en el get).
    4. Entorno formulario para la introducción de usuarios en el front.